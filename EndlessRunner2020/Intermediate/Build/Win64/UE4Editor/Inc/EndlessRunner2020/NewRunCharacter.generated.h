// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef ENDLESSRUNNER2020_NewRunCharacter_generated_h
#error "NewRunCharacter.generated.h already included, missing '#pragma once' in NewRunCharacter.h"
#endif
#define ENDLESSRUNNER2020_NewRunCharacter_generated_h

#define EndlessRunner2020_Source_EndlessRunner2020_NewRunCharacter_h_12_RPC_WRAPPERS
#define EndlessRunner2020_Source_EndlessRunner2020_NewRunCharacter_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define EndlessRunner2020_Source_EndlessRunner2020_NewRunCharacter_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesANewRunCharacter(); \
	friend struct Z_Construct_UClass_ANewRunCharacter_Statics; \
public: \
	DECLARE_CLASS(ANewRunCharacter, ACharacter, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/EndlessRunner2020"), NO_API) \
	DECLARE_SERIALIZER(ANewRunCharacter)


#define EndlessRunner2020_Source_EndlessRunner2020_NewRunCharacter_h_12_INCLASS \
private: \
	static void StaticRegisterNativesANewRunCharacter(); \
	friend struct Z_Construct_UClass_ANewRunCharacter_Statics; \
public: \
	DECLARE_CLASS(ANewRunCharacter, ACharacter, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/EndlessRunner2020"), NO_API) \
	DECLARE_SERIALIZER(ANewRunCharacter)


#define EndlessRunner2020_Source_EndlessRunner2020_NewRunCharacter_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ANewRunCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ANewRunCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ANewRunCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ANewRunCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ANewRunCharacter(ANewRunCharacter&&); \
	NO_API ANewRunCharacter(const ANewRunCharacter&); \
public:


#define EndlessRunner2020_Source_EndlessRunner2020_NewRunCharacter_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ANewRunCharacter(ANewRunCharacter&&); \
	NO_API ANewRunCharacter(const ANewRunCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ANewRunCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ANewRunCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ANewRunCharacter)


#define EndlessRunner2020_Source_EndlessRunner2020_NewRunCharacter_h_12_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__SkeletalMesh() { return STRUCT_OFFSET(ANewRunCharacter, SkeletalMesh); } \
	FORCEINLINE static uint32 __PPO__Camera() { return STRUCT_OFFSET(ANewRunCharacter, Camera); } \
	FORCEINLINE static uint32 __PPO__CameraArm() { return STRUCT_OFFSET(ANewRunCharacter, CameraArm); }


#define EndlessRunner2020_Source_EndlessRunner2020_NewRunCharacter_h_9_PROLOG
#define EndlessRunner2020_Source_EndlessRunner2020_NewRunCharacter_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	EndlessRunner2020_Source_EndlessRunner2020_NewRunCharacter_h_12_PRIVATE_PROPERTY_OFFSET \
	EndlessRunner2020_Source_EndlessRunner2020_NewRunCharacter_h_12_RPC_WRAPPERS \
	EndlessRunner2020_Source_EndlessRunner2020_NewRunCharacter_h_12_INCLASS \
	EndlessRunner2020_Source_EndlessRunner2020_NewRunCharacter_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define EndlessRunner2020_Source_EndlessRunner2020_NewRunCharacter_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	EndlessRunner2020_Source_EndlessRunner2020_NewRunCharacter_h_12_PRIVATE_PROPERTY_OFFSET \
	EndlessRunner2020_Source_EndlessRunner2020_NewRunCharacter_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	EndlessRunner2020_Source_EndlessRunner2020_NewRunCharacter_h_12_INCLASS_NO_PURE_DECLS \
	EndlessRunner2020_Source_EndlessRunner2020_NewRunCharacter_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID EndlessRunner2020_Source_EndlessRunner2020_NewRunCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
