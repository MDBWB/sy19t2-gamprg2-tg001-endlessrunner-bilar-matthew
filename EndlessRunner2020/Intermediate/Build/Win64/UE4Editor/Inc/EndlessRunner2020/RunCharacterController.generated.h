// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef ENDLESSRUNNER2020_RunCharacterController_generated_h
#error "RunCharacterController.generated.h already included, missing '#pragma once' in RunCharacterController.h"
#endif
#define ENDLESSRUNNER2020_RunCharacterController_generated_h

#define EndlessRunner2020_Source_EndlessRunner2020_RunCharacterController_h_15_RPC_WRAPPERS
#define EndlessRunner2020_Source_EndlessRunner2020_RunCharacterController_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define EndlessRunner2020_Source_EndlessRunner2020_RunCharacterController_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesARunCharacterController(); \
	friend struct Z_Construct_UClass_ARunCharacterController_Statics; \
public: \
	DECLARE_CLASS(ARunCharacterController, APlayerController, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/EndlessRunner2020"), NO_API) \
	DECLARE_SERIALIZER(ARunCharacterController)


#define EndlessRunner2020_Source_EndlessRunner2020_RunCharacterController_h_15_INCLASS \
private: \
	static void StaticRegisterNativesARunCharacterController(); \
	friend struct Z_Construct_UClass_ARunCharacterController_Statics; \
public: \
	DECLARE_CLASS(ARunCharacterController, APlayerController, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/EndlessRunner2020"), NO_API) \
	DECLARE_SERIALIZER(ARunCharacterController)


#define EndlessRunner2020_Source_EndlessRunner2020_RunCharacterController_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ARunCharacterController(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ARunCharacterController) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ARunCharacterController); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ARunCharacterController); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ARunCharacterController(ARunCharacterController&&); \
	NO_API ARunCharacterController(const ARunCharacterController&); \
public:


#define EndlessRunner2020_Source_EndlessRunner2020_RunCharacterController_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ARunCharacterController(ARunCharacterController&&); \
	NO_API ARunCharacterController(const ARunCharacterController&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ARunCharacterController); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ARunCharacterController); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ARunCharacterController)


#define EndlessRunner2020_Source_EndlessRunner2020_RunCharacterController_h_15_PRIVATE_PROPERTY_OFFSET
#define EndlessRunner2020_Source_EndlessRunner2020_RunCharacterController_h_12_PROLOG
#define EndlessRunner2020_Source_EndlessRunner2020_RunCharacterController_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	EndlessRunner2020_Source_EndlessRunner2020_RunCharacterController_h_15_PRIVATE_PROPERTY_OFFSET \
	EndlessRunner2020_Source_EndlessRunner2020_RunCharacterController_h_15_RPC_WRAPPERS \
	EndlessRunner2020_Source_EndlessRunner2020_RunCharacterController_h_15_INCLASS \
	EndlessRunner2020_Source_EndlessRunner2020_RunCharacterController_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define EndlessRunner2020_Source_EndlessRunner2020_RunCharacterController_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	EndlessRunner2020_Source_EndlessRunner2020_RunCharacterController_h_15_PRIVATE_PROPERTY_OFFSET \
	EndlessRunner2020_Source_EndlessRunner2020_RunCharacterController_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	EndlessRunner2020_Source_EndlessRunner2020_RunCharacterController_h_15_INCLASS_NO_PURE_DECLS \
	EndlessRunner2020_Source_EndlessRunner2020_RunCharacterController_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID EndlessRunner2020_Source_EndlessRunner2020_RunCharacterController_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
