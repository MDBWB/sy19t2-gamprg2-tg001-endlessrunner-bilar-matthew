// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class ACharacter;
#ifdef ENDLESSRUNNER2020_Tile_generated_h
#error "Tile.generated.h already included, missing '#pragma once' in Tile.h"
#endif
#define ENDLESSRUNNER2020_Tile_generated_h

#define EndlessRunner2020_Source_EndlessRunner2020_Tile_h_9_DELEGATE \
struct _Script_EndlessRunner2020_eventExited_Parms \
{ \
	ACharacter* tileExit; \
}; \
static inline void FExited_DelegateWrapper(const FMulticastScriptDelegate& Exited, ACharacter* tileExit) \
{ \
	_Script_EndlessRunner2020_eventExited_Parms Parms; \
	Parms.tileExit=tileExit; \
	Exited.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define EndlessRunner2020_Source_EndlessRunner2020_Tile_h_14_RPC_WRAPPERS
#define EndlessRunner2020_Source_EndlessRunner2020_Tile_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define EndlessRunner2020_Source_EndlessRunner2020_Tile_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesATile(); \
	friend struct Z_Construct_UClass_ATile_Statics; \
public: \
	DECLARE_CLASS(ATile, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/EndlessRunner2020"), NO_API) \
	DECLARE_SERIALIZER(ATile)


#define EndlessRunner2020_Source_EndlessRunner2020_Tile_h_14_INCLASS \
private: \
	static void StaticRegisterNativesATile(); \
	friend struct Z_Construct_UClass_ATile_Statics; \
public: \
	DECLARE_CLASS(ATile, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/EndlessRunner2020"), NO_API) \
	DECLARE_SERIALIZER(ATile)


#define EndlessRunner2020_Source_EndlessRunner2020_Tile_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ATile(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ATile) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATile); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATile(ATile&&); \
	NO_API ATile(const ATile&); \
public:


#define EndlessRunner2020_Source_EndlessRunner2020_Tile_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATile(ATile&&); \
	NO_API ATile(const ATile&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATile); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ATile)


#define EndlessRunner2020_Source_EndlessRunner2020_Tile_h_14_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__StaticMesh() { return STRUCT_OFFSET(ATile, StaticMesh); } \
	FORCEINLINE static uint32 __PPO__ExitTrigger() { return STRUCT_OFFSET(ATile, ExitTrigger); } \
	FORCEINLINE static uint32 __PPO__ArrowPoint() { return STRUCT_OFFSET(ATile, ArrowPoint); }


#define EndlessRunner2020_Source_EndlessRunner2020_Tile_h_11_PROLOG
#define EndlessRunner2020_Source_EndlessRunner2020_Tile_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	EndlessRunner2020_Source_EndlessRunner2020_Tile_h_14_PRIVATE_PROPERTY_OFFSET \
	EndlessRunner2020_Source_EndlessRunner2020_Tile_h_14_RPC_WRAPPERS \
	EndlessRunner2020_Source_EndlessRunner2020_Tile_h_14_INCLASS \
	EndlessRunner2020_Source_EndlessRunner2020_Tile_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define EndlessRunner2020_Source_EndlessRunner2020_Tile_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	EndlessRunner2020_Source_EndlessRunner2020_Tile_h_14_PRIVATE_PROPERTY_OFFSET \
	EndlessRunner2020_Source_EndlessRunner2020_Tile_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	EndlessRunner2020_Source_EndlessRunner2020_Tile_h_14_INCLASS_NO_PURE_DECLS \
	EndlessRunner2020_Source_EndlessRunner2020_Tile_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID EndlessRunner2020_Source_EndlessRunner2020_Tile_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
