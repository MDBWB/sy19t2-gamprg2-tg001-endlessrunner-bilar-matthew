// Fill out your copyright notice in the Description page of Project Settings.

#include "NewRunCharacter.h"
#include "Components/InputComponent.h"
#include "Camera/CameraComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "Engine/SkeletalMesh.h"
#include "GameFramework/SpringArmComponent.h"
#include "Components/CapsuleComponent.h"

// Sets default values
ANewRunCharacter::ANewRunCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SkeletalMesh = CreateDefaultSubobject<USkeletalMeshComponent>("SkeletalMesh");
	// Set the static mesh as the root component
	SetRootComponent(SkeletalMesh);

	// Setup camera arm
	CameraArm = CreateDefaultSubobject<USpringArmComponent>("CameraArm");
	CameraArm->SetupAttachment(GetCapsuleComponent());
	// Initial length (can be modified in the blueprint)
	CameraArm->TargetArmLength = 500.0f;

	// Setup camera
	Camera = CreateDefaultSubobject<UCameraComponent>("Camera");
	// Attach the camera to the spring arm. This allows the camera to move with the spring arm
	Camera->SetupAttachment(CameraArm);

}

// Called when the game starts or when spawned
void ANewRunCharacter::BeginPlay()
{
	Super::BeginPlay();
	SkeletalMesh->SetWorldScale3D(FMath::VRand());
}

// Called every frame
void ANewRunCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ANewRunCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

