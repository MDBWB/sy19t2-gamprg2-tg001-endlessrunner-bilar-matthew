// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "RunCharacterController.generated.h"

/**
 * 
 */
UCLASS()
class ENDLESSRUNNER2020_API ARunCharacterController : public APlayerController
{
	GENERATED_BODY()
	

public:
	ARunCharacterController();

protected:
	virtual void BeginPlay() override;
	void MoveForward(float scale);
	void MoveRight(float scale);

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupInputComponent() override;

	FVector CurrentVelocity;

private:
	class ANewRunCharacter* runCharacter;
};
